create table chat
(
    chat_id     bigint                                 not null primary key,
    state       text                                   not null,
    username    text,
    bot_status text,
    created_at  timestamp with time zone default now() not null,
    modified_at timestamp with time zone default now() not null
);
