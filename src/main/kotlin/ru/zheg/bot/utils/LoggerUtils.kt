package ru.zheg.bot.utils

import com.github.kotlintelegrambot.entities.User

object LoggerUtils {

    fun User.usernameOrId() = this.username?.let { "@$it" } ?: this.id
}
