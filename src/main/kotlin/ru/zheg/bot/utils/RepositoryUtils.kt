package ru.zheg.bot.utils

import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.InsertResultStep
import org.jooq.InsertValuesStepN
import org.jooq.Record
import org.jooq.UpdateResultStep
import org.jooq.impl.TableImpl

object RepositoryUtils {
    fun <R : Record> DSLContext.insertRecord(table: TableImpl<R>, record: R): InsertValuesStepN<R> {
        return this.insertInto(table, *record.fields())
            .values(record.values())
    }

    fun <R: Record> InsertResultStep<R>.fetchSingle(): R = checkNotNull(fetchOne())

    fun <R: Record> UpdateResultStep<R>.fetchSingle(): R = checkNotNull(fetchOne())

    fun Record.values(): List<Any> = this.fields().map { this.getValue(it) }

    fun <R : Record> DSLContext.insertRecordExceptFirstField(table: TableImpl<R>, record: R): InsertValuesStepN<R> {
        val fields = record.fields().drop(1)
        return this.insertInto(table, fields)
            .values(record.values(fields))
    }

    fun Record.values(fields: List<Field<*>>): List<Any> = fields.map { this.getValue(it) }
}
