package ru.zheg.bot.controllers

import mu.KLogging
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("\${bot.webhook.basePath:}")
class CheckController {

    @RequestMapping(
        method = [RequestMethod.POST],
        value = ["/check"],
    )
    fun check(): ResponseEntity<Unit> {
        logger.info { "received check request" }
        return ResponseEntity.ok().build()
    }

    private companion object : KLogging()
}
