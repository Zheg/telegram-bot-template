package ru.zheg.bot.controllers

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.Update
import kotlinx.coroutines.runBlocking
import mu.KLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@ConditionalOnProperty("bot.webhook.enabled")
@RequestMapping("\${bot.webhook.basePath:}")
class UpdateController(
    private val bot: Bot,
) {

    @RequestMapping(
        method = [RequestMethod.POST],
        consumes = ["application/json"],
    )
    fun update(@RequestBody update: Update): ResponseEntity<Unit> = runBlocking {
        logger.trace { "received update: $update" }
        bot.handleUpdate(update)
        ResponseEntity.ok().build()
    }

    private companion object : KLogging()
}
