package ru.zheg.bot.commands

import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.Message
import mu.KLogging
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import ru.zheg.bot.configs.BotProperty
import ru.zheg.bot.handlers.callbacks.Callback
import ru.zheg.bot.services.MessageService

@Component
class CheckCommand(
    @Lazy private val messageService: MessageService,
    private val botProperty: BotProperty,
) : Command {

    override val commandName: String = "check"
    override fun execute(message: Message, args: List<String>) {
        if (message.chat.id != botProperty.adminChatId) return

        logger.info { "Received check command." }
        val text = StringBuilder("Bot is working.")

        messageService.sendMessage(
            chatId = message.chat.id,
            text = text.toString(),
            replyMarkup = InlineKeyboardMarkup.createSingleButton(
                button = Callback.Type.DELETE_MESSAGE.toButton(args)
            )
        )
    }

    private companion object : KLogging()
}
