package ru.zheg.bot.commands

import com.github.kotlintelegrambot.dispatcher.handlers.CommandHandlerEnvironment
import com.github.kotlintelegrambot.entities.Message

interface Command {

    val commandName: String
    fun execute(message: Message, args: List<String>)

    fun execute(commandHandlerEnvironment: CommandHandlerEnvironment) {
        execute(message = commandHandlerEnvironment.message, args = commandHandlerEnvironment.args)
    }
}
