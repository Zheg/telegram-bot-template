package ru.zheg.bot.services

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.entities.ReplyMarkup
import mu.KLogging
import org.springframework.stereotype.Service

@Service
class MessageService(
    private val bot: Bot,
) {

    companion object : KLogging()

    fun sendMessage(
        chatId: Long,
        text: String,
        replyMarkup: ReplyMarkup? = null,
    ): Message? {
        logger.debug { "send message to chat = $chatId. Message content: $text" }
        val message = bot.sendMessage(
            chatId = ChatId.fromId(chatId),
            text = text,
            replyMarkup = replyMarkup,
        )

        return message.getOrNull()
    }

    fun deleteMessage(
        chatId: Long,
        messageId: Long,
    ) {
        bot.deleteMessage(chatId = ChatId.fromId(chatId), messageId = messageId)
    }
}
