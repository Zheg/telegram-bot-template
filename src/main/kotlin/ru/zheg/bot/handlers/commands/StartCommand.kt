package ru.zheg.bot.handlers.commands

import com.github.kotlintelegrambot.entities.Message
import org.springframework.stereotype.Component
import ru.zheg.bot.repositories.ChatRepository

@Component
class StartCommand(
    private val chatRepository: ChatRepository,
) : Command {

    override val commandName = "start"

    override fun execute(message: Message, args: List<String>) {

        chatRepository.create(
            chatId = message.chat.id,
            userName = message.chat.username
        )
    }
}
