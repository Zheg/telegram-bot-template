package ru.zheg.bot.handlers.my_chat_members

import com.github.kotlintelegrambot.entities.ChatMemberUpdated
import org.springframework.stereotype.Component
import ru.zheg.bot.models.Chat
import ru.zheg.bot.repositories.ChatRepository

@Component
class PrivateMyChatMember(
    private val chatRepository: ChatRepository,
) : MyChatMemberHandler {

    override val chatType = "private"

    override suspend fun execute(chatMemberUpdated: ChatMemberUpdated) {
        if (chatMemberUpdated.newChatMember.status == "kicked") {
            chatRepository.setBotStatus(
                chatId = chatMemberUpdated.chat.id,
                botStatus = Chat.BotStatus.BLOCKED
            )
        } else if (chatMemberUpdated.newChatMember.status == "member") {
            chatRepository.setBotStatus(
                chatId = chatMemberUpdated.chat.id,
                botStatus = null,
            )
        }
    }
}
