package ru.zheg.bot.handlers.my_chat_members

import com.github.kotlintelegrambot.dispatcher.handlers.MyChatMemberHandlerEnvironment
import com.github.kotlintelegrambot.entities.ChatMemberUpdated

interface MyChatMemberHandler {

    val chatType: String

    suspend fun execute(chatMemberUpdated: ChatMemberUpdated)

    suspend fun execute(myChatMemberHandlerEnvironment: MyChatMemberHandlerEnvironment) {
        execute(myChatMemberHandlerEnvironment.chatMemberUpdated)
        myChatMemberHandlerEnvironment.update.consume()
    }
}
