package ru.zheg.bot.handlers.callbacks

import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton

data class Callback(
    val type: Type,
    val arguments: List<String> = emptyList(),
    val text: String = type.text,
) {

    fun toButton() = InlineKeyboardButton.CallbackData(text, type.data(arguments))

    enum class Type(val text: String, val callbackId: String) {
        DELETE_MESSAGE("Удалить сообщение", "#del"),
        ;

        fun build(arguments: List<String> = emptyList(), text: String = this.text) =
            Callback(type = this, arguments = arguments, text = text)

        fun toButton(arguments: List<String> = emptyList(), text: String = this.text) =
            InlineKeyboardButton.CallbackData(text, data(arguments))

        fun data(arguments: List<String> = emptyList()): String {
            val value = StringBuilder(callbackId)
            arguments.forEach {
                value.append(" $it")
            }

            return value.toString()
        }

        companion object {

            fun getArguments(callbackData: String) = callbackData.split(" ").drop(1)
        }
    }
}
