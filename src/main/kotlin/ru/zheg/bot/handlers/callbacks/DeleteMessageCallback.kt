package ru.zheg.bot.handlers.callbacks

import com.github.kotlintelegrambot.dispatcher.handlers.CallbackQueryResponse
import com.github.kotlintelegrambot.entities.CallbackQuery
import com.github.kotlintelegrambot.entities.Message
import mu.KLogging
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import ru.zheg.bot.services.MessageService

@Component
class DeleteMessageCallback(
    @Lazy private val messageService: MessageService,
) : CallbackCommand {

    private companion object : KLogging()

    override suspend fun execute(
        callbackQuery: CallbackQuery,
        arguments: List<String>,
        message: Message,
    ): CallbackQueryResponse? {
        messageService.deleteMessage(
            chatId = callbackQuery.message!!.chat.id,
            messageId = callbackQuery.message.messageId
        )

        return null
    }

    override val id: String = Callback.Type.DELETE_MESSAGE.callbackId
}
