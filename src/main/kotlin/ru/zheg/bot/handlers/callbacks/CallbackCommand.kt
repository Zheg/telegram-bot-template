package ru.zheg.bot.handlers.callbacks

import com.github.kotlintelegrambot.dispatcher.handlers.CallbackQueryHandlerEnvironment
import com.github.kotlintelegrambot.dispatcher.handlers.CallbackQueryResponse
import com.github.kotlintelegrambot.entities.CallbackQuery
import com.github.kotlintelegrambot.entities.Message
import mu.KLogging
import ru.zheg.bot.utils.LoggerUtils.usernameOrId

interface CallbackCommand {

    private companion object : KLogging()

    suspend fun execute(callbackQueryHandlerEnvironment: CallbackQueryHandlerEnvironment): CallbackQueryResponse? {
        logger.trace { "Received callback: $callbackQueryHandlerEnvironment" }

        val message = callbackQueryHandlerEnvironment.callbackQuery.message
        if (message == null) {
            logger.warn { "message is null in callback query ${callbackQueryHandlerEnvironment.callbackQuery}. Do nothing on this update" }
            return null
        }

        val arguments = Callback.Type.getArguments(callbackQueryHandlerEnvironment.callbackQuery.data)
        logger.info { "received callback with id = $id, arguments = $arguments, from = ${callbackQueryHandlerEnvironment.callbackQuery.from.usernameOrId()}" }

        val callbackQueryResponse = execute(callbackQueryHandlerEnvironment.callbackQuery, arguments, message)
        callbackQueryHandlerEnvironment.update.consume()

        return callbackQueryResponse
    }

    suspend fun execute(callbackQuery: CallbackQuery, arguments: List<String>, message: Message): CallbackQueryResponse?

    val id: String
}
