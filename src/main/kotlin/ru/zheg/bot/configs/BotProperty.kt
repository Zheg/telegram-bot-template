package ru.zheg.bot.configs

import mu.KLogging
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
@ConfigurationProperties("bot")
data class BotProperty(
    var username: String = "",
    var token: String = "",
    var adminChatId: Long = 0,
    var webhook: WebhookConfig = WebhookConfig(),
) {

    @PostConstruct
    fun validate() {
        logger.debug { "bot property initialized with: $this" }
        if ((username == "" || username == "\${BOT_NAME}" || token == "" || token == "\${BOT_TOKEN}" || adminChatId == 0L)) {
            throw IllegalArgumentException("bots username, token and adminChatId must be set")
        }
        webhook.validate()
    }

    data class WebhookConfig(
        var enabled: Boolean = false,
        var hostname: String = "",
        var basePath: String = "",
    ) {

        @PostConstruct
        fun validate() {
            logger.debug { "webhook config initialized with: $this" }
            if (enabled && (hostname == "" || hostname == "\${HOSTNAME}" || basePath == "" || basePath == "\${BOT_NAME}")) {
                throw IllegalArgumentException("webhook's hostname and basePath must be set on enabled")
            }
        }
    }

    private companion object : KLogging()
}
