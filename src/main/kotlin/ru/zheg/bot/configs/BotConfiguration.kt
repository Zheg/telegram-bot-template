package ru.zheg.bot.configs

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.callbackQuery
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.logging.LogLevel
import com.github.kotlintelegrambot.network.bimap
import mu.KLogging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.zheg.bot.commands.Command
import ru.zheg.bot.handlers.callbacks.CallbackCommand

@Configuration
class BotConfiguration {

    private companion object : KLogging()

    @Bean
    fun botBean(
        botProperty: BotProperty,
        commands: List<Command>,
        callbackCommands: List<CallbackCommand>,
    ): Bot {
        val bot = bot {
            token = botProperty.token
            logLevel = LogLevel.Error
            dispatch {
                commands.forEach {
                    command(it.commandName) {
                        it.execute(this)
                    }
                }
                callbackCommands.forEach {
                    callbackQuery(data = it.id) {
                        it.execute(this)
                    }
                }
            }
        }

        with(botProperty.webhook) {
            if (enabled) {
                logger.info { "trying to start webhook" }
                val setWebhookResult = bot.setWebhook(url = "${hostname}/${basePath}")
                val webhookSet = setWebhookResult.bimap(
                    mapResponse = { true },
                    mapError = { false }
                )

                if (!webhookSet) {
                    logger.info { "It's failed to start. Starting polling" }
                    bot.startPolling()
                }
            } else {
                logger.info { "Webhook is disabled. Starting polling" }
                bot.startPolling()
            }
        }

        return bot
    }
}
