package ru.zheg.bot.configs

import com.github.kotlintelegrambot.network.serialization.GsonFactory
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.GsonHttpMessageConverter

@Configuration
class GsonConfiguration {

    @Bean
    fun httpMessageConverters(): HttpMessageConverters {
        val gsonHttpMessageConverter = GsonHttpMessageConverter()
        gsonHttpMessageConverter.gson = GsonFactory.createForApiClient()
        return HttpMessageConverters(true, listOf(gsonHttpMessageConverter))
    }
}
