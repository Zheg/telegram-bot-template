package ru.zheg.bot.models

data class Chat(
    val chatId: Long,
    val userName: String?,
    val state: State,
    val botStatus: BotStatus?,
) {

    enum class State {
        NONE,
        WIN,
        BAN,
    }

    enum class BotStatus {
        DEACTIVATED,
        BLOCKED,
        UNKNOWN,
    }
}
