package ru.zheg.bot.repositories

import mu.KLogging
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import ru.zheg.bot.db.tables.records.ChatRecord
import ru.zheg.bot.db.tables.references.CHAT
import ru.zheg.bot.models.Chat
import ru.zheg.bot.utils.RepositoryUtils.fetchSingle
import ru.zheg.bot.utils.RepositoryUtils.insertRecord
import java.time.Instant

@Repository
class ChatRepository(
    private val dslContext: DSLContext,
) {

    companion object : KLogging()

    fun create(
        chatId: Long,
        userName: String?,
        state: Chat.State = Chat.State.NONE,
        botStatus: Chat.BotStatus? = null,
    ): Chat {
        logger.info { "create new user userName = $userName, chatId = $chatId" }
        val record = ChatRecord(
            chatId = chatId,
            state = state.name,
            username = userName,
            createdAt = Instant.now(),
            modifiedAt = Instant.now(),
        )
        return dslContext.insertRecord(CHAT, record)
            .onDuplicateKeyUpdate()
            .set(CHAT.STATE, state.name)
            .set(CHAT.USERNAME, userName)
            .set(CHAT.MODIFIED_AT, Instant.now())
            .returning().fetchSingle().toData()
    }

    fun getState(chatId: Long): Chat.State? {
        logger.debug { "get state for chatId = $chatId" }
        return getUser(chatId)?.state
    }

    private fun getUser(chatId: Long): Chat? {
        return dslContext.selectFrom(CHAT)
            .where(CHAT.CHAT_ID.eq(chatId))
            .fetchOne()?.toData()
    }

    fun updateState(chatId: Long, userName: String?, state: Chat.State = Chat.State.NONE): Chat {
        logger.debug { "set chatId = $chatId state = $state" }

        return dslContext.update(CHAT)
            .set(CHAT.STATE, state.name)
            .set(CHAT.MODIFIED_AT, Instant.now())
            .where(CHAT.CHAT_ID.eq(chatId))
            .returning().fetchOne()?.toData()
            ?: return create(chatId = chatId, userName = userName, state = state)
    }

    fun filterBanned(chatIds: Set<Long>): Set<Long> {
        return dslContext.selectFrom(CHAT)
            .where(CHAT.CHAT_ID.`in`(chatIds))
            .and(CHAT.STATE.eq(Chat.State.BAN.name))
            .fetchSet(CHAT.CHAT_ID, Long::class.java)
    }

    fun findPaginated(chatIdOffset: Long, limit: Int): List<Chat> {
        logger.debug { "fetching paginated users chatIdOffset = $chatIdOffset, limit = $limit" }
        return dslContext.selectFrom(CHAT)
            .where(CHAT.CHAT_ID.greaterThan(chatIdOffset))
            .orderBy(CHAT.CHAT_ID)
            .limit(limit)
            .fetch().map { it.toData() }
    }

    fun setBotStatus(chatId: Long, botStatus: Chat.BotStatus?): Chat {
        logger.debug { "set chatId = $chatId, botStatus = $botStatus" }
        return dslContext.update(CHAT)
            .set(CHAT.BOT_STATUS, botStatus?.name)
            .set(CHAT.MODIFIED_AT, Instant.now())
            .where(CHAT.CHAT_ID.eq(chatId))
            .returning().fetchOne()?.toData()
            ?: create(chatId = chatId, userName = null, botStatus = botStatus)
    }

    fun ChatRecord.toData() = Chat(
        userName = username,
        chatId = checkNotNull(chatId),
        state = Chat.State.valueOf(checkNotNull(state)),
        botStatus = botStatus?.let { Chat.BotStatus.valueOf(it) },
    )
}
