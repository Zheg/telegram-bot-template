val postgresVersion = "42.5.1"
val logging_interceptor_version = "3.8.0"
val retrofit_version = "2.9.0"
val junit_version = "5.10.0"
val mock_web_server_version = "4.2.1"

plugins {
    id("nu.studer.jooq") version ("8.1")
    id("org.springframework.boot") version "2.5.6"
    id("io.spring.dependency-management") version "1.1.0"
    kotlin("plugin.spring") version "1.8.10"
    kotlin("jvm") version "1.8.10"
    application
}

group = "ru.zheg"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    // Database
    jooqGenerator("org.postgresql:postgresql:$postgresVersion")
    runtimeOnly("org.postgresql:postgresql")

    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")

    // Классические стартеры spring boot
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.springframework.boot:spring-boot-starter-web")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

    // Serialization
    implementation("com.google.code.gson:gson")

    // Logging
    implementation("io.github.microutils:kotlin-logging-jvm:2.1.20")

    // --------------- start of libraries needed to telegram lib --------------------------
    implementation("com.squareup.okhttp3:logging-interceptor:$logging_interceptor_version")
    // Networking
    implementation("com.squareup.retrofit2:retrofit:$retrofit_version")
    implementation("com.squareup.retrofit2:converter-gson:$retrofit_version")
    implementation("com.google.code.findbugs:jsr305:3.0.2")
    // --------------- end of libraries needed to telegram lib ----------------------------

    // Client engine
    implementation("com.squareup.okhttp3:okhttp:4.10.0")

    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$junit_version")
    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("io.mockk:mockk:1.10.2")
    testImplementation("com.squareup.okhttp3:mockwebserver:$mock_web_server_version")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.2")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

kotlin.sourceSets["main"].kotlin {
    srcDir("${projectDir}/src/jooq/kotlin")
}

jooq {
    edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)

    configurations {
        create("main") {
            generateSchemaSourceOnCompilation.set(false)
            jooqConfiguration.apply {
                jdbc.apply {
                    driver = "org.postgresql.Driver"
                    url = "jdbc:postgresql://localhost:${System.getenv("OPEN_PORT")}/"
                    user = System.getenv("PG_USER")
                    password = System.getenv("PG_PASS")
                }
                generator.apply {
                    name = "org.jooq.codegen.KotlinGenerator"
                    generate.apply {
                        isDeprecated = false
                        isRecords = true
                        isKotlinSetterJvmNameAnnotationsOnIsPrefix = true
                        isJavaTimeTypes = true
                    }
                    database.apply {
                        name = "org.jooq.meta.postgres.PostgresDatabase"
                        inputSchema = "public"
                        excludes = "flyway_schema_history"
                        forcedTypes = listOf(
                            org.jooq.meta.jaxb.ForcedType().apply {
                                name = "INSTANT"
                                includeTypes = "(?i:TIMESTAMP\\ WITH\\ TIME\\ ZONE)"
                            }
                        )
                    }
                    target.apply {
                        // Пакет куда отрпавляются сгенерированные классы
                        packageName = "ru.zheg.bot.db"
                        directory = "${projectDir}/src/jooq/kotlin"
                    }
                    strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
                }
            }
        }
    }
}

tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq").configure {
    inputs.files(fileTree("db/migration"))
        .withPropertyName("migrations")
        .withPathSensitivity(PathSensitivity.RELATIVE)
    allInputsDeclared.set(true)
    outputs.upToDateWhen { false }
}
