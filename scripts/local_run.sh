#!/bin/bash

set -o allexport
source "scripts/local.env"
set +o allexport

./scripts/local_deploy_db.sh skip_read
./gradlew :bootRun --args='--spring.profiles.active=local'

read
