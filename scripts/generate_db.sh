#!/bin/bash

set -o allexport
source "scripts/local.env"
set +o allexport

./scripts/deploy_db.sh "$@"
./gradlew :generateJooq

for arg in "$@"; do
  if [[ "$arg" == "--local" ]]; then
    docker stop ${BOT_NAME}db
    docker rm -v ${BOT_NAME}db
    break
  fi
done
