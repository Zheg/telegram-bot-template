#!/bin/bash

set -o allexport
source "scripts/local.env"
set +o allexport

./scripts/local_deploy_db.sh skip_read

./gradlew :generateJooq

docker stop ${BOT_NAME}db
docker rm -v ${BOT_NAME}db

read
