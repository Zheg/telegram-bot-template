#!/bin/bash

env_file=app.env
generate_env() {
  echo "generate $env_file"
  echo "" >$env_file
  for variable in $APPLICATION_VARIABLES; do
    value="$(eval "echo \$$variable")"
    echo "$variable=$value" >>$env_file
  done
}

container_name=${BOT_NAME}
image_name=${CI_REGISTRY_IMAGE}

echo "deploy container = $container_name from image = $image_name"

# grep old image id
old_image_id=$(docker image ls | grep "$image_name" | awk '{print $3}')
echo "old_image_id=$old_image_id"

# pull new image
echo "docker pull $image_name"
docker pull "$image_name"

# we need new image id to check if it's equal to old, and if not delete the old image
new_image_id=$(docker image ls | grep "$image_name" | awk '{print $3}')
echo "new_image_id=$new_image_id"

# stop old container and remove it with its volume
echo "stopping old container"
docker stop "$container_name"
docker logs "$container_name" >&/var/bot-logs/"$container_name"_"$(date +%Y-%m-%d_%H:%M:%S)".log
docker rm -v "$container_name"

# start new container
echo "starting new container"
generate_env
docker run --publish "$OPEN_PORT_BOT:8080" \
  --network "${BOT_NAME}network" \
  --restart unless-stopped \
  --name "$container_name" \
  --env-file $env_file \
  -d "$image_name"

# delete the old image if it isn't equal to the new one
if [[ "$old_image_id" != "$new_image_id" ]]; then
  echo "remove old image $old_image_id"
  docker rmi "$old_image_id"
fi
