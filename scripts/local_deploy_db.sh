#!/bin/bash

set -o allexport
source "scripts/local.env"
set +o allexport

docker network create ${BOT_NAME}network

sed -iBACKUP '/postgres -c config_file=\/etc\/postgresql\.conf/d; /BOT_NAME_pgdata\:\/var\/lib\/postgresql\/data/d' db/docker-compose-template.yaml

./scripts/deploy_db.sh

cat db/docker-compose-template.yamlBACKUP >db/docker-compose-template.yaml
rm db/docker-compose-template.yamlBACKUP

if [ -z "$1" ]; then
  read
fi
