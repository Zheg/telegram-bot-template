#!/bin/sh

# build new image
echo "building new image with name ${CI_REGISTRY_IMAGE}"

./gradlew :clean :bootBuildImage --imageName="${CI_REGISTRY_IMAGE}"
echo "push ${CI_REGISTRY_IMAGE}"
docker push "${CI_REGISTRY_IMAGE}"
