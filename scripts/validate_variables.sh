#!/bin/bash

if [ -z "${!APPLICATION_VARIABLES}" ]; then
  echo "Variable \$$APPLICATION_VARIABLES is empty or not set"
  exit 1
fi

for variable in $APPLICATION_VARIABLES; do
  if [ -z "${!variable}" ]; then
    echo "Variable \$$variable is empty or not set"
    exit 1
  fi
done

if echo "$BOT_NAME" | grep -q '^[a-z]*$'; then
  echo "BOT_NAME contains only lowercase letters - ok"
else
  echo "BOT_NAME does not contain only lowercase letters"
  exit 1
fi

echo "All variables set"
