#!/bin/bash

echo "Deploy database for bot = ${BOT_NAME}"

cd db || exit 1

sed -e "s/BOT_NAME_/${BOT_NAME}/g; s/OPEN_PORT/$OPEN_PORT/g" docker-compose-template.yaml >"docker-compose.yaml"

docker compose up --wait "${BOT_NAME}db"
docker compose up "${BOT_NAME}flyway"

rm "docker-compose.yaml"
